//Chat box
function appendChat(e) {
    var id = e.target.id;
    var notEmpty = $(".usertext").val() != "",
        isEnterKeypress = e.type == "keypress" && e.keyCode == 13,
        isSendClick = e.type == "click" && id == "send";

    if( notEmpty && (isEnterKeypress || isSendClick) ) {
		var txt = "<b>me: </b>"+$(".usertext").val();
		var rply = ["<b>admin: </b> Butuh bantuan?",
						"<b>admin: </b> Iya",
						"<b>admin: </b> Baik",
						"<b>admin: </b> Tidak",
						"<b>admin: </b> APASIH LU GAJE",
						"<b>admin: </b> Menyerahlah",
						"<b>admin: </b> Mengesalkan bukan?",
						]
		var min = 0;
		var max = 6;
		var random = Math.floor(Math.random() * (max - min + 1)) + min;
		$('<p>'+txt+'</p>').addClass('msg-send').appendTo('.msg-insert');
		$(".usertext").val("");
		$('<p>'+rply[random]+'</p>').addClass('msg-receive').appendTo('.msg-insert');
		$(".chat-body").scrollTop($(".chat-body").height());
    }
}
$(".chav_box_in").keypress(appendChat);
//end Chat Box

/*var themes ="[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]";*/

// var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};*/


// Calculator
var print = document.getElementById('print');
var displayValue = '0';  
var erase = false;

var go = function(x) {
	if (x === 'ac') {
		print.value = '';
	}
	else if (x === 'log') {
		print.value = Math.log(x);
	}
	else if (x === 'tan') {
		print.value = Math.tan(x);
	}
	else if (x === 'sin') {
		print.value = Math.sin(x);
	}
	else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	}
	else {
		print.value += x;
	}
	$('#print').val(print.value).html();

};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END Calculator

	 // $('.my-select').select2();