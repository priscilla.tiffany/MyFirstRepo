function appendChat(e) {
    var id = e.target.id;
    //variables for testing, you could have all of the
    //comparisons in the 'if' statement, just using these to
    //make the 'if' statement more clear
    var notEmpty = $(".usertext").val() != "",
        isEnterKeypress = e.type == "keypress" && e.keyCode == 13,
        isSendClick = e.type == "click" && id == "send";

    if( notEmpty && (isEnterKeypress || isSendClick) ) {
       var txt = "<b>me:</b>"+$(".usertext").val();
       $("#chatlog").append($("<li>").html(txt));
       $(".usertext").val("");
    }
}


$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini
  
	$("#send").click(appendChat);
	$(".chav_box_in").keypress(appendChat);
});