# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-15 01:53
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0015_auto_20171114_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 15, 8, 53, 13, 349778, tzinfo=utc)),
        ),
    ]
