# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-14 15:01
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0013_auto_20171114_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 11, 14, 22, 1, 30, 250066, tzinfo=utc)),
        ),
    ]
