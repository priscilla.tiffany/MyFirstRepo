from django.db import models
from django.utils import timezone
import pytz

# Create your models here.
class Message(models.Model):
    def indonesianTZ():
        return timezone.now() + timezone.timedelta(hours=7)

    name = models.CharField(max_length=27)
    email = models.EmailField()
    message = models.TextField()
    created_date = models.DateTimeField(default=indonesianTZ())

    def __str__(self):
        return self.message