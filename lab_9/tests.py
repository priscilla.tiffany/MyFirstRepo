import environ, requests
from django.test import TestCase, Client
from django.urls import resolve, reverse

from .views import index, set_data_for_session, profile, add_session_drones, del_session_drones, clear_session_drones, cookie_login, cookie_auth_login,cookie_profile, cookie_clear, my_cookie_auth, is_login, add_session_item, del_session_item, clear_session_item
from .api_enterkomputer import get_drones, get_soundcard, get_optical
from .csui_helper import get_access_token, get_client_id, verify_user, get_data_user
from .custom_auth import auth_login, auth_logout

env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env()
API_VERIFY_USER = "https://akun.cs.ui.ac.id/oauth/token/verify/"
API_MAHASISWA = "https://api-dev.cs.ui.ac.id/siakngcs/mahasiswa/"

class Lab9UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")
        self.npm = "1606822964"

    def test_lab_9_url_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_lab9_using_right_template(self):
        #if not logged in
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_9/session/login.html')
        #logged in
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_9/session/profile.html')

    def test_profile(self):
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('priscilla.tiffany',html_response)

    def test_profile_not_login(self):
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

# ======================================================================== #
    # Test api_enterkomputer.py
    def test_drones_api(self):
        response = requests.get('https://www.enterkomputer.com/api/product/drone.json')
        self.assertEqual(response.json(),get_drones().json())

    def test_soundcards_api(self):
        response = requests.get('https://www.enterkomputer.com/api/product/soundcard.json')
        self.assertEqual(response.json(),get_soundcard().json())

    def test_opticals_api(self):
        response = requests.get('https://www.enterkomputer.com/api/product/optical.json')
        self.assertEqual(response.json(),get_optical().json())
#=============================================================================#
    #test custom_auth.py

    def test_fail_login(self):
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': 'kylie.jenner', 'password': 'haduh'})
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertRaises(Exception, auth_login)

    def test_logout_auth(self):
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        self.assertEqual(response_post.status_code, 302)
        response = self.client.post(reverse('lab-9:auth_logout'))
        html_response = self.client.get('/lab-9/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn('Anda berhasil logout. Semua session Anda sudah dihapus',html_response)

#============================================================================================#
    #Test csui_helper
    def test_username_and_pass_wrong(self):
        username = "kylie.jenner"
        password = "haduh"
        with self.assertRaises(Exception) as context:
            get_access_token(username, password)
        self.assertIn("kylie.jenner", str(context.exception))

    def test_verify_function(self):
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_VERIFY_USER, params=parameters)
        result = verify_user(access_token)
        self.assertEqual(result,response.json())

    def test_get_data_user_function(self):
        access_token = get_access_token(self.username,self.password)
        parameters = {"access_token": access_token, "client_id": get_client_id()}
        response = requests.get(API_MAHASISWA+self.npm, params=parameters)
        result = get_data_user(access_token,self.npm)
        self.assertEqual(result,response.json())

#=============================================================================#
    #drones
    def test_add_favorite_drone(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107894})) #item DJI Mavic Battery
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107893})) #item DJI Phantom 3 Battery
        response_post = self.client.post(reverse('lab-9:profile'))

        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')

        self.assertIn('Hapus dari favorit', html_response)

    def test_delete_favorite_drone(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107894})) #item DJI Mavic Battery
        response_post = self.client.post(reverse('lab-9:profile'))
        #delete items
        response_post = self.client.post(reverse('lab-9:del_session_drones', kwargs={'id':107894}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')

        self.assertIn('Favoritkan',html_response)

    def test_reset_favorite_drone(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_drones', kwargs={'id':107894})) #item DJI Mavic Battery
        response_post = self.client.post(reverse('lab-9:profile'))
        #reset items
        response_post = self.client.post(reverse('lab-9:clear_session_drones'))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')

        self.assertIn('Favoritkan',html_response)


#=============================================================================#
    #soundcards
    def test_add_favorite_soundcard(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'soundcard','id':53495})) #item Power Color Devil HDX PCI-E Internal
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'soundcard','id':53496}))
        response_post = self.client.post(reverse('lab-9:profile'))

        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')

        self.assertIn('Hapus dari favorit', html_response)

    def test_delete_favorite_soundcard(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'soundcard','id':53496})) #item Power Color Devil HDX PCI-E Internal
        response_post = self.client.post(reverse('lab-9:profile'))
        #delete items
        response_post = self.client.post(reverse('lab-9:del_session_item', kwargs={'key':'soundcard','id':53496}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Favoritkan',html_response)

    def test_reset_favorite_soundcard(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'soundcard','id':53496})) #item Power Color Devil HDX PCI-E Internal
        #reset items
        response_post = self.client.post(reverse('lab-9:clear_session_item',kwargs={'key':'soundcard'}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Favoritkan',html_response)

    
#=============================================================================#
    #optical
    def test_add_favorite_optical(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'optical','id':4459})) #item DJI Mavic Battery
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'optical','id':4460}))
        response_post = self.client.post(reverse('lab-9:profile'))

        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')

        self.assertIn('Hapus dari favorit', html_response)

    def test_delete_favorite_optical(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'optical','id':4459})) #item DJI Mavic Battery
        response_post = self.client.post(reverse('lab-9:profile'))
        #delete items
        response_post = self.client.post(reverse('lab-9:del_session_item', kwargs={'key':'optical','id':4459}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Favoritkan',html_response)

    def test_reset_favorite_optical(self):
        #login
        response_post = self.client.post(reverse('lab-9:auth_login'), {'username': self.username, 'password': self.password})
        #test tambahkan favorit drones
        response_post = self.client.post(reverse('lab-9:add_session_item', kwargs={'key':'optical','id':4459})) #item DJI Mavic Battery
        response_post = self.client.post(reverse('lab-9:profile'))
        #reset items
        response_post = self.client.post(reverse('lab-9:clear_session_item',kwargs={'key':'optical'}))
        response = self.client.get('/lab-9/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn('Favoritkan',html_response)

#=================================================================================#
#cookie

    def test_cookie(self):
        #not logged in
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 302)

        #login using HTTP GET method
        response = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(response.status_code, 302)

        #login failed, invalid pass and uname
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'u', 'password': 'p'})
        html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)

        #try to set manual cookies
        self.client.cookies.load({"user_login": "u", "user_password": "p"})
        response = self.client.get('/lab-9/cookie/profile/')
        html_response = response.content.decode('utf-8')

        #login successed
        self.client = Client()
        response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/login/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(response.status_code, 200)

        #logout
        response = self.client.post('/lab-9/cookie/clear/')
        html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)